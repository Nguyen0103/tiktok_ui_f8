import React from "react"
import PropTypes from 'prop-types'
import Button from "~/components/Button/Button"

import classNames from "classnames/bind"
import styles from './Menu.module.scss'

const cx = classNames.bind(styles)

function MenuItems({ data, onClick }) {
    const { title, icon, to } = data
    const classes = cx('menu-item', {
        separate: data.separate,
    })
    return (
        <Button className={classes} leftIcon={icon} to={to} onClick={onClick}>
            {title}
        </Button>
    )
}

MenuItems.propTypes = {
    data: PropTypes.object.isRequired,
    onClick: PropTypes.func,
}


export default MenuItems