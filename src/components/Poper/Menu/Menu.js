import Tippy from '@tippyjs/react/headless'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'
import React, { useState } from 'react'
import styles from './Menu.module.scss'
import { Wrapper as PoperWrapper } from '~/components/Poper'
import MenuItems from './MenuItems'
import MenuHeader from './MenuHeader'

const cx = classNames.bind(styles)

const defaultFn = () => { }

function Menu({ children, items = [], hideOnClick = false, onChange = defaultFn }) {

    const [history, setHistory] = useState([{ data: items }]);
    const CurrentMenu = history[history.length - 1]

    // Reset to Frist level 
    const handleResetMenu = () => {
        setHistory(prev => prev.slice(0, 1))
    }

    const handleBack = () => {
        setHistory(prev => prev.slice(0, prev.length - 1))
    }

    const renderResult = (attrs) => (
        <div className={cx('menu-list')} tabIndex="-1" {...attrs}>
            <PoperWrapper className={cx('menu-Poper')}>
                {history.length > 1 && <MenuHeader title={CurrentMenu.title} onBack={handleBack} />}
                <div className={cx('menu-body')}>
                    {renderItems()}
                </div>
            </PoperWrapper>
        </div>
    )

    const renderItems = () => {
        return CurrentMenu.data.map((item, index) => {
            const isParent = !!item.children
            return < MenuItems key={index} data={item} onClick={() => {
                if (isParent) {
                    setHistory(prev => [...prev, item.children])
                } else {
                    onChange(item)
                }
            }} />
        })
    }

    return (
        <Tippy
            delay={[0, 300]}
            offset={[12, 8]}
            interactive
            hideOnClick={hideOnClick}
            placement='bottom-end'
            render={renderResult}
            onHide={handleResetMenu}
        >
            {children}
        </Tippy>
    )
}

Menu.propTypes = {
    children: PropTypes.node.isRequired,
    items: PropTypes.array,
    hideOnClick: PropTypes.bool,
    onChange: PropTypes.func,
}

export default Menu
