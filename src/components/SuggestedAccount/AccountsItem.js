import React from 'react'
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames/bind'
import Tippy from '@tippyjs/react/headless';
import { Wrapper as PoperWrapper } from '~/components/Poper'

import styles from './SuggestedAccounts.module.scss'
import AccountPreview from './AccountsPreview/AccountPreview';

const cx = classNames.bind(styles)

function AccountsItem() {

    const renderPreview = (props) => {
        return (
            <div className={cx('preview')} tabIndex='-1' {...props}>
                <PoperWrapper>
                    <AccountPreview />
                </PoperWrapper>
            </div>
        )
    }

    return (
        <div>
            <Tippy interactive delay={[800, 0]} placement='bottom' offset={[-20, 0]} render={renderPreview}>
                <div className={cx('account-item')}>
                    <img
                        className={cx('avatar')}
                        src="https://p16-sign-va.tiktokcdn.com/tos-useast2a-avt-0068-aiso/65d3c6b1d1e205c75536ccf1f26d552d~c5_100x100.jpeg?x-expires=1668909600&x-signature=4KYP5kSOz4s25fQsIIFGmomMsic%3D"
                        alt="avatar" />
                    <div className={cx('item-info')}>
                        <h4 className={cx('nickname')}>
                            <strong>theanh28 entertainment</strong>
                            <FontAwesomeIcon className={cx('check')} icon={faCheckCircle} />
                        </h4>
                        <p className={cx('name')}>Theanh28 Entertainment</p>
                    </div>
                </div>
            </Tippy>
        </div>
    )
}

AccountsItem.propTypes = {

}

export default AccountsItem
