import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames/bind'
import styles from './SuggestedAccounts.module.scss'
import AccountsItem from './AccountsItem';


const cx = classNames.bind(styles)

function SuggestedAccounts({ label }) {
    return (
        <div className={cx('wrapper')}>
            <p className={cx('label')}>{label}</p>
            <AccountsItem />
            <AccountsItem />
            <AccountsItem />
            <div className={cx('wrapper-more-btn')}>
                <p className={cx('more-btn')}>See all</p>
            </div>
        </div>
    )
}

SuggestedAccounts.propTypes = {
    label: PropTypes.string.isRequired,
}

export default SuggestedAccounts
