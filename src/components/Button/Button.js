import React from 'react'
import Proptype from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames/bind'

import styles from './Button.module.scss'

const cx = classNames.bind(styles)

function Button({
    // Navigate
    to,
    href,
    // styles
    primary = false,
    outline = false,
    text = false,
    disabled = false,
    rounded = false,
    // Size
    small = false,
    medium = false,
    large = false,
    // content
    children,
    className,
    // IconPosition
    leftIcon,
    rightIcon,
    // actions
    onClick,
    // another
    ...passProps
}) {
    let Component = `button`
    const props = {
        onClick,
        ...passProps,
    }
    const classes = cx('wrapper', {
        // styles
        primary,
        outline,
        disabled,
        text,
        rounded,
        // sizes
        small,
        medium,
        large,
        //content
        [className]: className,
    })

    // Remove eventsListener when  button is disbled
    if (disabled) {
        Object.keys(props).forEach(key => {
            if (key.startsWith('on') && typeof props[key] === 'function') {
                delete props[key];
            }
        })
    }

    if (to) {
        props.to = to
        Component = Link
    } else {
        props.href = href
        Component = "a"
    }

    return (
        <Component className={classes} {...props}>
            {leftIcon && <span className={cx('icon')}>{leftIcon}</span>}
            <span className={cx('title')}>
                {children}
            </span>
            {rightIcon && <span className={cx('icon')}>{rightIcon}</span>}
        </Component>
    )
}

Button.Proptype = {
    to: Proptype.string,
    href: Proptype.string,
    className: Proptype.string,

    primary: Proptype.bool,
    outline: Proptype.bool,
    text: Proptype.bool,
    disabled: Proptype.bool,
    rounded: Proptype.bool,
    small: Proptype.bool,
    medium: Proptype.bool,
    large: Proptype.bool,

    children: Proptype.node.isRequired,
    leftIcon: Proptype.node,
    rightIcon: Proptype.node,

    onClick: Proptype.func,

}


export default Button