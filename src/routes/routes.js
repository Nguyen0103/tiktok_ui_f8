import config from '~/config/config'


//Layout
import { HeaderOnly } from "~/layouts";


import Home from "~/Pages/Home/Home";
import Following from "~/Pages/Following/Following";
import Profile from "~/Pages/Profile/Profile";
import Upload from "~/Pages/Upload/Upload";
import Search from "~/Pages/Search/Search";
import Live from '~/Pages/Live/Live';

// this array is page don't needed login to use
const publicRoutes = [
    { path: config.routes.home, component: Home, },
    { path: config.routes.following, component: Following, },
    { path: config.routes.profile, component: Profile, },
    { path: config.routes.live, component: Live, },
    { path: config.routes.upload, component: Upload, layout: HeaderOnly },
    { path: config.routes.search, component: Search, layout: null },
    { path: config.routes.search, component: Search, layout: null },
]

// this array is page needed login to use
const privateRoutes = [

]


export { publicRoutes, privateRoutes }