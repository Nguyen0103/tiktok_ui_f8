import React from 'react'
import classNames from 'classnames/bind'
import PropTypes from 'prop-types'

import Header from '~/layouts/components/Header/Header'
import SideBar from '~/layouts/components/SideBar/SideBar'

import styles from './DefaultLayout.module.scss'

const cx = classNames.bind(styles)

function DefaultLayout({ children }) {
    return (
        <div className={cx('wrapper')}>
            <Header />
            <div className={cx('container')}>
                <SideBar />
                <div className={cx('content')}>
                    {children}
                </div>
            </div>
        </div>
    )
}


DefaultLayout.prototype = {
    children: PropTypes.node.isRequired
}

export default DefaultLayout