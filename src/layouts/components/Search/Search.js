import React, { useEffect, useState, useRef } from 'react'
import { faCircleXmark, faSpinner } from '@fortawesome/free-solid-svg-icons'; // FontAwesome
import HeadlessTippy from '@tippyjs/react/headless'; // Tippy/headless
import { Wrapper as PoperWrapper } from '~/components/Poper/'
import SearchAccountItem from '~/components/AccountItem/SearchAccountItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { SearchIcon } from '~/components/Icons/Icons';
import classNames from 'classnames/bind';
import * as searchService from '~/Services/searchService';

// custom Hook
import { useDebounce } from '~/Hooks';

import styles from './Search.module.scss'

const cx = classNames.bind(styles)

export default function Search() {

    const [searchValue, setSearchValue] = useState('');
    const [searchResult, SetSearchResult] = useState([]);
    const [showResult, setShowResult] = useState(false);
    const [loading, setLoading] = useState(false);

    const debouncedValue = useDebounce(searchValue, 500)

    const inputRef = useRef()



    const handleClear = () => {
        setSearchValue('');
        setShowResult([]);
        inputRef.current.focus();
    }

    const handleOnHiddeResult = () => {
        setShowResult(false)
    }

    const handleChange = (e) => {
        const searchValue = e.target.value
        if (!searchValue.startsWith(' ')) {
            setSearchValue(searchValue)
        }
    }

    useEffect(() => {
        if (!debouncedValue.trim()) {
            SetSearchResult([])
            return
        }

        const fetchAPI = async () => {
            setLoading(true)
            const result = await searchService.searchService(debouncedValue)
            SetSearchResult(result)
            setLoading(false)
        }
        fetchAPI()
    }, [debouncedValue])

    return (
        // Using a wrapper <Div> or <Span> tag around the reference element solves this by creating a new parentNode context
        <div>
            <HeadlessTippy
                interactive
                visible={showResult && searchResult.length > 0}
                render={attrs => (
                    <div className={cx('search-result')} tabIndex='-1' {...attrs}>
                        <PoperWrapper>
                            <h4 className={cx('search-title')}>Account</h4>
                            {searchResult.map((result) => (
                                <SearchAccountItem key={result.id} data={result} />
                            ))}
                        </PoperWrapper>
                    </div>
                )}
                onClickOutside={handleOnHiddeResult}
            >
                <div className={cx('search')}>
                    <input
                        ref={inputRef}
                        value={searchValue}
                        placeholder='search account and videos...'
                        spellCheck={false}
                        onChange={handleChange}
                        onFocus={() => setShowResult(true)}
                    />

                    {!!searchValue && !loading && (
                        <button className={cx('clear')} onClick={handleClear}>
                            <FontAwesomeIcon icon={faCircleXmark} />
                        </button>
                    )}

                    {loading && <FontAwesomeIcon className={cx('loading')} icon={faSpinner} />}


                    <button className={cx('search-btn')} onMouseDown={e => e.preventDefault()}>
                        <SearchIcon />
                    </button>
                </div>
            </HeadlessTippy>
        </div>

    )
}
