import React from 'react'
import classNames from 'classnames/bind'
import MenuSideBar, { MenuItemSideBar } from './Menu'
import styles from './SideBar.module.scss'
import config from '~/config/config'

import { HomeIcon, UserGroupIcon, LiveIcon, HomeActiveIcon, UserGroupActiveIcon, LiveActiveIcon } from '~/components/Icons/Icons'

import SuggestedAccount from '~/components/SuggestedAccount'

const cx = classNames.bind(styles)

export default function SideBar() {
    return (
        <aside className={cx('wrapper')}>
            <MenuSideBar>
                <MenuItemSideBar title="For You" to={config.routes.home} icon={<HomeIcon />} activeIcon={<HomeActiveIcon />} />
                <MenuItemSideBar title="Following" to={config.routes.following} icon={<UserGroupIcon />} activeIcon={<UserGroupActiveIcon />} />
                <MenuItemSideBar title="LIVE" to={config.routes.live} icon={<LiveIcon />} activeIcon={<LiveActiveIcon />} />
            </MenuSideBar>

            <SuggestedAccount label='Suggested accounts' />
            <SuggestedAccount label='following accounts' />
        </aside>
    )
}
