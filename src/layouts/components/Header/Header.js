import React from 'react';
// Lib
import classNames from 'classnames/bind'; //classNames
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCircleQuestion,
    faCoins,
    faEarthAsia,
    faEllipsisVertical,
    faGear,
    faKeyboard,
    faSignOut,
} from '@fortawesome/free-solid-svg-icons'; // FontAwesome
import { Link } from 'react-router-dom'; // RouterDom
import Tippy from '@tippyjs/react'; // Tippy

import 'tippy.js/dist/tippy.css'; //Tippy Css

// Templates Import
import styles from './Header.module.scss';
import images from '~/assets/images';
import config from '~/config/config'
import Button from '~/components/Button/Button'
import Menu from '~/components/Poper/Menu/Menu';
import Image from '~/components/Image/Image';
import Search from '../Search/Search'

// IconsSVG
import { InboxIcon, MessageIcon, UploadIcon } from '~/components/Icons/Icons';

const cx = classNames.bind(styles)

export default function Header() {

    const currentUser = true;

    //handle Logic
    const handleMenuChange = (menuItem) => {
        switch (menuItem.type) {
            case 'language':
                //HandleChangeLanguage
                break
            default:
                return
        }

    }



    const MENU_ITEMS = [
        {
            icon: <FontAwesomeIcon icon={faEarthAsia} />,
            title: 'English',
            //submenu
            children: {
                title: 'Language',
                data: [
                    {
                        type: 'language',
                        code: 'cn',
                        title: 'China',
                    },
                    {
                        type: 'language',
                        code: 'vi',
                        title: 'Vietnamese',
                    },
                    {
                        type: 'language',
                        code: 'en',
                        title: 'English',
                    },
                    {
                        type: 'language',
                        code: 'jp',
                        title: 'Japan',
                    },
                ]
            }
        },
        {
            icon: <FontAwesomeIcon icon={faCircleQuestion} />,
            title: 'Feedback and help',
            to: '/feedback'
        },
        {
            icon: <FontAwesomeIcon icon={faKeyboard} />,
            title: 'Keyboard Shortcuts',

        },
    ]

    const MENU_USER = [
        {
            icon: <FontAwesomeIcon icon={faCircleQuestion} />,
            title: 'View Profile',
            to: '/@hoaa'
        },
        {
            icon: <FontAwesomeIcon icon={faCoins} />,
            title: 'Get Coins',
            to: '/coin',
        },
        {
            icon: <FontAwesomeIcon icon={faGear} />,
            title: 'Settings',
            to: '/settings',
        },
        ...MENU_ITEMS,
        {
            icon: <FontAwesomeIcon icon={faSignOut} />,
            title: 'Log out',
            to: '/logout',
            separate: true,
        }
    ]

    return (
        <header className={cx('wrapper')}>
            <div className={cx('inner')}>

                <Link to={config.routes.home} className={cx('logo')}>
                    <img src={images.logo} alt="tiktok" />
                </Link>

                <Search />

                <div className={cx('actions')}>
                    {currentUser ? (
                        <>
                            <Tippy delay={[0, 50]} content='upload video' placement='bottom'>
                                <button className={cx('actions-btn')}>
                                    <UploadIcon />
                                </button>
                            </Tippy>
                            <Tippy delay={[0, 50]} content='Message' placement='bottom'>
                                <button className={cx('actions-btn')}>
                                    <MessageIcon />
                                </button>
                            </Tippy>
                            <Tippy delay={[0, 50]} content='Inbox' placement='bottom'>
                                <button className={cx('actions-btn')}>
                                    <InboxIcon />
                                    <span className={cx('badge')}>12</span>
                                </button>
                            </Tippy>
                        </>
                    ) : (
                        <>
                            <Button text>Upload</Button>
                            <Button primary>Log in</Button>
                        </>
                    )}

                    <Menu items={currentUser ? MENU_USER : MENU_ITEMS} onChange={handleMenuChange}>
                        {currentUser
                            ? (
                                <Image
                                    src='https://p16-sign-va.tiktokcdn.com/tos-useast2a-avt-0068-giso/b9e1fb6031f81c5eb0d63a9dbbb1da27~c5_100x100.jpeg?x-expires=1667995200&x-signature=IiT2E4AtJbtO9KpZombuWyO%2FY14%3D'
                                    alt='Someonename'
                                    className={cx('user-avatar')}
                                    fallback="https://fullstack.edu.vn/static/media/f8-icon.18cd71cfcfa33566a22b.png"
                                />
                            )
                            : (
                                <button className={cx('more-btn')}>
                                    <FontAwesomeIcon icon={faEllipsisVertical} />
                                </button>
                            )
                        }
                    </Menu>
                </div>
            </div>
        </header >
    )
}
